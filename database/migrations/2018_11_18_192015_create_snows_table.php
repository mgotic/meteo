<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSnowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('snows', function (Blueprint $table) {
            $table->increments('id');
            $table->date('snow_date');
            $table->time('snow_time');
            $table->integer('location_id');
            $table->float('snow_amount');
            $table->float('snow_amount_new');
            $table->timestamps();

            $table->index(['snow_date, snow_time, location_id'],'snow_composite_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('snows');
    }
}
