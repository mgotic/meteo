<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrecipitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('precipitations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('precipitation_date');
            $table->time('precipitation_time');
            $table->integer('location_id');
            $table->float('precipitation_observed');
            $table->timestamps();

            // $table->unique('precipitation_date', 'precipitation_time', 'location_id');
            $table->index(['precipitation_date, precipitation_time, location_id'],'precipitation_composite_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precipitations');
    }
}
