<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('suns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->default(1);
            $table->date('date');
            $table->time('sunrise');
            $table->time('sunset');
            $table->time('moonrise')->nullable();
            $table->time('moonset')->nullable();
            $table->time('moonphase')->nullable();
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations');
            $table->unique('location_id', 'date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suns');
    }
}
