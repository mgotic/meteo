<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeaTemperaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('sea_temperatures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->integer('station_type_id');
            $table->date('date');
            $table->integer('time');
            $table->float('sea_temperature');
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('station_type_id')->references('id')->on('station_types');

            $table->index(['location_id, date, time'],'sea_temperature_composite_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sea_temperatures');
    }
}
