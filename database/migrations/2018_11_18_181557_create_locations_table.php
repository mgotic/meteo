<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('namesc');
            $table->string('image')->nullable();
            $table->string('desc')->nullable();
            $table->string('www')->nullable();
            $table->string('country')->default('HRV'); // ISO 3166-1
            $table->string('gps')->nullable();
            $table->integer('masl')->nullable(); // meters above sea level 
            $table->integer('sun_id')->nullable(); // sunrise / sunset...
            $table->timestamps();

            // $table->foreign('sun_id')->references('id')->on('suns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
