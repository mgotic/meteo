<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('forecasts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('forecast_type_id'); // from sources
            $table->string('naslov');
            $table->string('tekst');
            $table->string('nastao');
            $table->dateTime('forecest_for'); // for date, from naslov field
            $table->dateTime('forecest_from'); // frecast published at, form nastao field
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecasts');
    }
}
