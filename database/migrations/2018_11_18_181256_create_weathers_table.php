<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlite_data')->create('weathers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('weather_date');
            $table->time('weather_time');
            $table->integer('location_id');
            $table->integer('station_type_id')->default(1); // automatic station od manual entry (SYNOP)
            $table->string('wind_direction')->nullable();
            $table->string('wind_speed')->nullable();
            $table->float('air_temperature');
            $table->integer('relative_humidity')->nullable();
            $table->float('atmospheric_pressure')->nullable();
            $table->float('pressure_tendency')->nullable();
            $table->integer('weather_condition_id');
            $table->string('weather_icon')->nullable();
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('station_type_id')->references('id')->on('station_types');
            $table->foreign('weather_condition_id')->references('id')->on('weather_conditions');

            $table->index(['weather_date, weather_time, location_id'], 'weather_composite_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weathers');
    }
}
