<?php

use Illuminate\Database\Seeder;

class StationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('sqlite_data')->table('station_types')->insert([
            'id' => 0,
            'code' => '0',
            'desc' => 'Podaci meteoroloških postaja (SYNOP)'
        ]);
        
        DB::connection('sqlite_data')->table('station_types')->insert([
            'id' => 1,
            'code' => '1',
            'desc' => 'Podaci automatskih postaja'
        ]);
    }
}
