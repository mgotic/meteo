<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class ProdSeedMissingXmls extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * $md5 = md5($xml);
     *  $sha1 = sha1($xml);
     *  $source_id = intval($url->id);
     * "1"	"ACTR"	"Vrijeme u Hrvatskoj (regije)"
     * "2"	"ACTA"	"Vrijeme u Hrvatskoj (abecedno)"
     * php artisan db:seed --class=ProdSeedMissingXmls
     */
    public function run()
    {
        $no = 1;
        $content1 = '
        <Hrvatska>
            <DatumTermin>
                <Datum>28.01.2018</Datum>
                <Termin>20</Termin>
            </DatumTermin>
            <Grad autom="0">
                <GradIme>Zagreb-Maksimir</GradIme>
                <Lat>45.822</Lat>
                <Lon>16.034</Lon>
                <Podatci>
                    <Temp>4.0</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Grič</GradIme>
                <Lat>45.814</Lat>
                <Lon>15.972</Lon>
                <Podatci>
                    <Temp>4.0</Temp>
                    <Vlaga>91</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Puntijarka</GradIme>
                <Lat>45.908</Lat>
                <Lon>15.968</Lon>
                <Podatci>
                    <Temp>5.8</Temp>
                    <Vlaga>71</Vlaga>
                    <Tlak>915.5*</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Krapina</GradIme>
                <Lat>46.138</Lat>
                <Lon>15.888</Lon>
                <Podatci>
                    <Temp>3.8</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Varaždin</GradIme>
                <Lat>46.283</Lat>
                <Lon>16.364</Lon>
                <Podatci>
                    <Temp>2.8</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>2.7</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Križevci</GradIme>
                <Lat>46.029</Lat>
                <Lon>16.554</Lon>
                <Podatci>
                    <Temp>3.5</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1035.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.1</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Bilogora</GradIme>
                <Lat>45.884</Lat>
                <Lon>17.200</Lon>
                <Podatci>
                    <Temp>1.8</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Daruvar</GradIme>
                <Lat>45.592</Lat>
                <Lon>17.210</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Bjelovar</GradIme>
                <Lat>45.910</Lat>
                <Lon>16.869</Lon>
                <Podatci>
                    <Temp>3.6</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Sisak</GradIme>
                <Lat>45.499</Lat>
                <Lon>16.367</Lon>
                <Podatci>
                    <Temp>4.6</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gorice (kod Nove Gradiške)</GradIme>
                <Lat>45.224</Lat>
                <Lon>17.278</Lon>
                <Podatci>
                    <Temp>2.5</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Kutjevo</GradIme>
                <Lat>45.424</Lat>
                <Lon>17.876</Lon>
                <Podatci>
                    <Temp>1.1</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1036.5</Tlak>
                    <TlakTend>-0.5</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Slavonski Brod</GradIme>
                <Lat>45.159</Lat>
                <Lon>17.995</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>magla uz vidljivo nebo</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Osijek-Čepin</GradIme>
                <Lat>45.503</Lat>
                <Lon>18.561</Lon>
                <Podatci>
                    <Temp>2.4</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gradište (kod Županje)</GradIme>
                <Lat>45.159</Lat>
                <Lon>18.704</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-0.4</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>magla uz vidljivo nebo</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Karlovac</GradIme>
                <Lat>45.494</Lat>
                <Lon>15.565</Lon>
                <Podatci>
                    <Temp>0.6</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ogulin</GradIme>
                <Lat>45.263</Lat>
                <Lon>15.222</Lon>
                <Podatci>
                    <Temp>1.2</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>NP Plitvička jezera</GradIme>
                <Lat>44.881</Lat>
                <Lon>15.620</Lon>
                <Podatci>
                    <Temp>3.7</Temp>
                    <Vlaga>81</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>+0.5</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.5</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Gospić</GradIme>
                <Lat>44.551</Lat>
                <Lon>15.373</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Parg-Čabar</GradIme>
                <Lat>45.594</Lat>
                <Lon>14.631</Lon>
                <Podatci>
                    <Temp>3.2</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>930.7*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Crikvenica</GradIme>
                <Lat>45.173</Lat>
                <Lon>14.689</Lon>
                <Podatci>
                    <Temp>7.5</Temp>
                    <Vlaga>84</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rijeka</GradIme>
                <Lat>45.337</Lat>
                <Lon>14.443</Lon>
                <Podatci>
                    <Temp>8.2</Temp>
                    <Vlaga>91</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Pazin</GradIme>
                <Lat>45.241</Lat>
                <Lon>13.945</Lon>
                <Podatci>
                    <Temp>5.3</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.9</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Porer - svjetionik</GradIme>
                <Lat>44.758</Lat>
                <Lon>13.890</Lon>
                <Podatci>
                    <Temp>9.7</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>3.7</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>RC Monte Kope</GradIme>
                <Lat>44.812</Lat>
                <Lon>13.873</Lon>
                <Podatci>
                    <Temp>9.3</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>povjetarac</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Ivan na pučini - svjetionik</GradIme>
                <Lat>45.043</Lat>
                <Lon>13.614</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>98</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Opatija</GradIme>
                <Lat>45.341</Lat>
                <Lon>14.313</Lon>
                <Podatci>
                    <Temp>8.1</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1032.0</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Malinska</GradIme>
                <Lat>45.126</Lat>
                <Lon>14.527</Lon>
                <Podatci>
                    <Temp>7.3</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.9</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Senj</GradIme>
                <Lat>44.993</Lat>
                <Lon>14.903</Lon>
                <Podatci>
                    <Temp>9.3</Temp>
                    <Vlaga>76</Vlaga>
                    <Tlak>1033.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zavižan</GradIme>
                <Lat>44.815</Lat>
                <Lon>14.975</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>41</Vlaga>
                    <Tlak>851.1*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rab</GradIme>
                <Lat>44.756</Lat>
                <Lon>14.769</Lon>
                <Podatci>
                    <Temp>8.6</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Mali Lošinj</GradIme>
                <Lat>44.533</Lat>
                <Lon>14.472</Lon>
                <Podatci>
                    <Temp>9.0</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zadar</GradIme>
                <Lat>44.130</Lat>
                <Lon>15.206</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Veli Rat - svjetionik</GradIme>
                <Lat>44.152</Lat>
                <Lon>14.820</Lon>
                <Podatci>
                    <Temp>10.4</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>+0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>3.9</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Knin</GradIme>
                <Lat>44.041</Lat>
                <Lon>16.207</Lon>
                <Podatci>
                    <Temp>5.9</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Šibenik</GradIme>
                <Lat>43.728</Lat>
                <Lon>15.906</Lon>
                <Podatci>
                    <Temp>8.4</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Split-Marjan</GradIme>
                <Lat>43.508</Lat>
                <Lon>16.426</Lon>
                <Podatci>
                    <Temp>10.4</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Makarska</GradIme>
                <Lat>43.288</Lat>
                <Lon>17.020</Lon>
                <Podatci>
                    <Temp>10.0</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1032.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Jure-Biokovo</GradIme>
                <Lat>43.342</Lat>
                <Lon>17.053</Lon>
                <Podatci>
                    <Temp>3.1</Temp>
                    <Vlaga>27</Vlaga>
                    <Tlak>835.4*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>5.6</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Hvar</GradIme>
                <Lat>43.171</Lat>
                <Lon>16.437</Lon>
                <Podatci>
                    <Temp>11.3</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.8</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Komiža</GradIme>
                <Lat>43.048</Lat>
                <Lon>16.085</Lon>
                <Podatci>
                    <Temp>12.1</Temp>
                    <Vlaga>82</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ploče</GradIme>
                <Lat>43.048</Lat>
                <Lon>17.443</Lon>
                <Podatci>
                    <Temp>6.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Dubrovnik</GradIme>
                <Lat>42.645</Lat>
                <Lon>18.085</Lon>
                <Podatci>
                    <Temp>9.5</Temp>
                    <Vlaga>78</Vlaga>
                    <Tlak>1032.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Prevlaka</GradIme>
                <Lat>42.393</Lat>
                <Lon>18.531</Lon>
                <Podatci>
                    <Temp>8.3</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>+0.3</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Lastovo</GradIme>
                <Lat>42.768</Lat>
                <Lon>16.900</Lon>
                <Podatci>
                    <Temp>10.5</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>8.3</VjetarBrzina>
                    <Vrijeme>umjereno oblačno, vjetrovito</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Palagruža</GradIme>
                <Lat>42.393</Lat>
                <Lon>16.255</Lon>
                <Podatci>
                    <Temp>11.3</Temp>
                    <Vlaga>77</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>5.9</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
        </Hrvatska>';

        DB::connection('sqlite')->table('xmls')->insert([
            'source_id' => 1, // 'ACTR',
            'content' => $content1,
            'md5' => md5($content1),
            'sha1' => sha1($content1),
            'loaded' => '0',
            'created_at' => date('Y-m-d H:i:s', mktime('20','00','00','01','28','2018')),
            'updated_at' => date('Y-m-d H:i:s', mktime('20','00','00','01','28','2018'))
        ]);
        Log::debug('Done Seeder: ProdSeedMissingXmls: ACTR, no: ' . $no);
        $no++;

        $content2 = '<?xml version="1.0" encoding="UTF-8"?>
        <Hrvatska>
            <DatumTermin>
                <Datum>28.01.2018</Datum>
                <Termin>20</Termin>
            </DatumTermin>
            <Grad autom="0">
                <GradIme>RC Bilogora</GradIme>
                <Lat>45.884</Lat>
                <Lon>17.200</Lon>
                <Podatci>
                    <Temp>1.8</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Bjelovar</GradIme>
                <Lat>45.910</Lat>
                <Lon>16.869</Lon>
                <Podatci>
                    <Temp>3.6</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Crikvenica</GradIme>
                <Lat>45.173</Lat>
                <Lon>14.689</Lon>
                <Podatci>
                    <Temp>7.5</Temp>
                    <Vlaga>84</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Daruvar</GradIme>
                <Lat>45.592</Lat>
                <Lon>17.210</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Dubrovnik</GradIme>
                <Lat>42.645</Lat>
                <Lon>18.085</Lon>
                <Podatci>
                    <Temp>9.5</Temp>
                    <Vlaga>78</Vlaga>
                    <Tlak>1032.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Gospić</GradIme>
                <Lat>44.551</Lat>
                <Lon>15.373</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gorice (kod Nove Gradiške)</GradIme>
                <Lat>45.224</Lat>
                <Lon>17.278</Lon>
                <Podatci>
                    <Temp>2.5</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gradište (kod Županje)</GradIme>
                <Lat>45.159</Lat>
                <Lon>18.704</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-0.4</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>magla uz vidljivo nebo</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Hvar</GradIme>
                <Lat>43.171</Lat>
                <Lon>16.437</Lon>
                <Podatci>
                    <Temp>11.3</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.8</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Karlovac</GradIme>
                <Lat>45.494</Lat>
                <Lon>15.565</Lon>
                <Podatci>
                    <Temp>0.6</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Knin</GradIme>
                <Lat>44.041</Lat>
                <Lon>16.207</Lon>
                <Podatci>
                    <Temp>5.9</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Komiža</GradIme>
                <Lat>43.048</Lat>
                <Lon>16.085</Lon>
                <Podatci>
                    <Temp>12.1</Temp>
                    <Vlaga>82</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Krapina</GradIme>
                <Lat>46.138</Lat>
                <Lon>15.888</Lon>
                <Podatci>
                    <Temp>3.8</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Križevci</GradIme>
                <Lat>46.029</Lat>
                <Lon>16.554</Lon>
                <Podatci>
                    <Temp>3.5</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1035.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.1</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Kutjevo</GradIme>
                <Lat>45.424</Lat>
                <Lon>17.876</Lon>
                <Podatci>
                    <Temp>1.1</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1036.5</Tlak>
                    <TlakTend>-0.5</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Lastovo</GradIme>
                <Lat>42.768</Lat>
                <Lon>16.900</Lon>
                <Podatci>
                    <Temp>10.5</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>8.3</VjetarBrzina>
                    <Vrijeme>umjereno oblačno, vjetrovito</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Malinska</GradIme>
                <Lat>45.126</Lat>
                <Lon>14.527</Lon>
                <Podatci>
                    <Temp>7.3</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.9</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Makarska</GradIme>
                <Lat>43.288</Lat>
                <Lon>17.020</Lon>
                <Podatci>
                    <Temp>10.0</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1032.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Mali Lošinj</GradIme>
                <Lat>44.533</Lat>
                <Lon>14.472</Lon>
                <Podatci>
                    <Temp>9.0</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>RC Monte Kope</GradIme>
                <Lat>44.812</Lat>
                <Lon>13.873</Lon>
                <Podatci>
                    <Temp>9.3</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>povjetarac</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ogulin</GradIme>
                <Lat>45.263</Lat>
                <Lon>15.222</Lon>
                <Podatci>
                    <Temp>1.2</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Opatija</GradIme>
                <Lat>45.341</Lat>
                <Lon>14.313</Lon>
                <Podatci>
                    <Temp>8.1</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1032.0</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Osijek-Čepin</GradIme>
                <Lat>45.503</Lat>
                <Lon>18.561</Lon>
                <Podatci>
                    <Temp>2.4</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Palagruža</GradIme>
                <Lat>42.393</Lat>
                <Lon>16.255</Lon>
                <Podatci>
                    <Temp>11.3</Temp>
                    <Vlaga>77</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>5.9</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Parg-Čabar</GradIme>
                <Lat>45.594</Lat>
                <Lon>14.631</Lon>
                <Podatci>
                    <Temp>3.2</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>930.7*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Pazin</GradIme>
                <Lat>45.241</Lat>
                <Lon>13.945</Lon>
                <Podatci>
                    <Temp>5.3</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.9</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>NP Plitvička jezera</GradIme>
                <Lat>44.881</Lat>
                <Lon>15.620</Lon>
                <Podatci>
                    <Temp>3.7</Temp>
                    <Vlaga>81</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>+0.5</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.5</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ploče</GradIme>
                <Lat>43.048</Lat>
                <Lon>17.443</Lon>
                <Podatci>
                    <Temp>6.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Porer - svjetionik</GradIme>
                <Lat>44.758</Lat>
                <Lon>13.890</Lon>
                <Podatci>
                    <Temp>9.7</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>3.7</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Prevlaka</GradIme>
                <Lat>42.393</Lat>
                <Lon>18.531</Lon>
                <Podatci>
                    <Temp>8.3</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>+0.3</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Puntijarka</GradIme>
                <Lat>45.908</Lat>
                <Lon>15.968</Lon>
                <Podatci>
                    <Temp>5.8</Temp>
                    <Vlaga>71</Vlaga>
                    <Tlak>915.5*</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rab</GradIme>
                <Lat>44.756</Lat>
                <Lon>14.769</Lon>
                <Podatci>
                    <Temp>8.6</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rijeka</GradIme>
                <Lat>45.337</Lat>
                <Lon>14.443</Lon>
                <Podatci>
                    <Temp>8.2</Temp>
                    <Vlaga>91</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Ivan na pučini - svjetionik</GradIme>
                <Lat>45.043</Lat>
                <Lon>13.614</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>98</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.2</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Senj</GradIme>
                <Lat>44.993</Lat>
                <Lon>14.903</Lon>
                <Podatci>
                    <Temp>9.3</Temp>
                    <Vlaga>76</Vlaga>
                    <Tlak>1033.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Sisak</GradIme>
                <Lat>45.499</Lat>
                <Lon>16.367</Lon>
                <Podatci>
                    <Temp>4.6</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Slavonski Brod</GradIme>
                <Lat>45.159</Lat>
                <Lon>17.995</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>magla uz vidljivo nebo</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Split-Marjan</GradIme>
                <Lat>43.508</Lat>
                <Lon>16.426</Lon>
                <Podatci>
                    <Temp>10.4</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.6</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Jure-Biokovo</GradIme>
                <Lat>43.342</Lat>
                <Lon>17.053</Lon>
                <Podatci>
                    <Temp>3.1</Temp>
                    <Vlaga>27</Vlaga>
                    <Tlak>835.4*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>5.6</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Šibenik</GradIme>
                <Lat>43.728</Lat>
                <Lon>15.906</Lon>
                <Podatci>
                    <Temp>8.4</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Varaždin</GradIme>
                <Lat>46.283</Lat>
                <Lon>16.364</Lon>
                <Podatci>
                    <Temp>2.8</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>2.7</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Veli Rat - svjetionik</GradIme>
                <Lat>44.152</Lat>
                <Lon>14.820</Lon>
                <Podatci>
                    <Temp>10.4</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>+0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>3.9</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zadar</GradIme>
                <Lat>44.130</Lat>
                <Lon>15.206</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Grič</GradIme>
                <Lat>45.814</Lat>
                <Lon>15.972</Lon>
                <Podatci>
                    <Temp>4.0</Temp>
                    <Vlaga>91</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Maksimir</GradIme>
                <Lat>45.822</Lat>
                <Lon>16.034</Lon>
                <Podatci>
                    <Temp>4.0</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.2</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zavižan</GradIme>
                <Lat>44.815</Lat>
                <Lon>14.975</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>41</Vlaga>
                    <Tlak>851.1*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
        </Hrvatska>';
		
        DB::connection('sqlite')->table('xmls')->insert([
            'source_id' => 2, // 'ACTA',
            'content' => $content2,
            'md5' => md5($content2),
            'sha1' => sha1($content2),
            'loaded' => '0',
            'created_at' => date('Y-m-d H:i:s', mktime('20','00','00','01','28','2018')),
            'updated_at' => date('Y-m-d H:i:s', mktime('20','00','00','01','28','2018'))
        ]);
        Log::debug('Done Seeder: ProdSeedMissingXmls: ACTA, no: ' . $no);
        $no++;

        $content3 = '
        <Hrvatska>
            <DatumTermin>
                <Datum>28.01.2018</Datum>
                <Termin>21</Termin>
            </DatumTermin>
            <Grad autom="0">
                <GradIme>Zagreb-Maksimir</GradIme>
                <Lat>45.822</Lat>
                <Lon>16.034</Lon>
                <Podatci>
                    <Temp>3.9</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-0.3</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Grič</GradIme>
                <Lat>45.814</Lat>
                <Lon>15.972</Lon>
                <Podatci>
                    <Temp>4.2</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Puntijarka</GradIme>
                <Lat>45.908</Lat>
                <Lon>15.968</Lon>
                <Podatci>
                    <Temp>5.2</Temp>
                    <Vlaga>80</Vlaga>
                    <Tlak>915.5*</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>3.2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Krapina</GradIme>
                <Lat>46.138</Lat>
                <Lon>15.888</Lon>
                <Podatci>
                    <Temp>3.4</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Varaždin</GradIme>
                <Lat>46.283</Lat>
                <Lon>16.364</Lon>
                <Podatci>
                    <Temp>2.9</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2.4</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Križevci</GradIme>
                <Lat>46.029</Lat>
                <Lon>16.554</Lon>
                <Podatci>
                    <Temp>3.4</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Bilogora</GradIme>
                <Lat>45.884</Lat>
                <Lon>17.200</Lon>
                <Podatci>
                    <Temp>1.8</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.1</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Daruvar</GradIme>
                <Lat>45.592</Lat>
                <Lon>17.210</Lon>
                <Podatci>
                    <Temp>2.0</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.0</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Bjelovar</GradIme>
                <Lat>45.910</Lat>
                <Lon>16.869</Lon>
                <Podatci>
                    <Temp>3.5</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Sisak</GradIme>
                <Lat>45.499</Lat>
                <Lon>16.367</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.5</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gorice (kod Nove Gradiške)</GradIme>
                <Lat>45.224</Lat>
                <Lon>17.278</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-0.6</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Kutjevo</GradIme>
                <Lat>45.424</Lat>
                <Lon>17.876</Lon>
                <Podatci>
                    <Temp>0.7</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1036.5</Tlak>
                    <TlakTend>-0.3</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Slavonski Brod</GradIme>
                <Lat>45.159</Lat>
                <Lon>17.995</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Osijek-Čepin</GradIme>
                <Lat>45.503</Lat>
                <Lon>18.561</Lon>
                <Podatci>
                    <Temp>2.4</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>-0.4</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2.3</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gradište (kod Županje)</GradIme>
                <Lat>45.159</Lat>
                <Lon>18.704</Lon>
                <Podatci>
                    <Temp>2.7</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Karlovac</GradIme>
                <Lat>45.494</Lat>
                <Lon>15.565</Lon>
                <Podatci>
                    <Temp>0.1</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ogulin</GradIme>
                <Lat>45.263</Lat>
                <Lon>15.222</Lon>
                <Podatci>
                    <Temp>0.8</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>0.1</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>NP Plitvička jezera</GradIme>
                <Lat>44.881</Lat>
                <Lon>15.620</Lon>
                <Podatci>
                    <Temp>3.0</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Gospić</GradIme>
                <Lat>44.551</Lat>
                <Lon>15.373</Lon>
                <Podatci>
                    <Temp>1.3</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1035.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Parg-Čabar</GradIme>
                <Lat>45.594</Lat>
                <Lon>14.631</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>930.7*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.9</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Crikvenica</GradIme>
                <Lat>45.173</Lat>
                <Lon>14.689</Lon>
                <Podatci>
                    <Temp>7.2</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.2</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rijeka</GradIme>
                <Lat>45.337</Lat>
                <Lon>14.443</Lon>
                <Podatci>
                    <Temp>7.4</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Pazin</GradIme>
                <Lat>45.241</Lat>
                <Lon>13.945</Lon>
                <Podatci>
                    <Temp>4.9</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Porer - svjetionik</GradIme>
                <Lat>44.758</Lat>
                <Lon>13.890</Lon>
                <Podatci>
                    <Temp>9.8</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.4</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>RC Monte Kope</GradIme>
                <Lat>44.812</Lat>
                <Lon>13.873</Lon>
                <Podatci>
                    <Temp>9.4</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2.0</VjetarBrzina>
                    <Vrijeme>povjetarac</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Ivan na pučini - svjetionik</GradIme>
                <Lat>45.043</Lat>
                <Lon>13.614</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>98</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>0.6</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Opatija</GradIme>
                <Lat>45.341</Lat>
                <Lon>14.313</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1031.9</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Malinska</GradIme>
                <Lat>45.126</Lat>
                <Lon>14.527</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Senj</GradIme>
                <Lat>44.993</Lat>
                <Lon>14.903</Lon>
                <Podatci>
                    <Temp>8.5</Temp>
                    <Vlaga>77</Vlaga>
                    <Tlak>1033.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zavižan</GradIme>
                <Lat>44.815</Lat>
                <Lon>14.975</Lon>
                <Podatci>
                    <Temp>4.8</Temp>
                    <Vlaga>38</Vlaga>
                    <Tlak>851.3*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.7</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rab</GradIme>
                <Lat>44.756</Lat>
                <Lon>14.769</Lon>
                <Podatci>
                    <Temp>8.0</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1033.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Mali Lošinj</GradIme>
                <Lat>44.533</Lat>
                <Lon>14.472</Lon>
                <Podatci>
                    <Temp>8.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zadar</GradIme>
                <Lat>44.130</Lat>
                <Lon>15.206</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Veli Rat - svjetionik</GradIme>
                <Lat>44.152</Lat>
                <Lon>14.820</Lon>
                <Podatci>
                    <Temp>10.5</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.8</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Knin</GradIme>
                <Lat>44.041</Lat>
                <Lon>16.207</Lon>
                <Podatci>
                    <Temp>5.5</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1033.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Šibenik</GradIme>
                <Lat>43.728</Lat>
                <Lon>15.906</Lon>
                <Podatci>
                    <Temp>8.0</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1033.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Split-Marjan</GradIme>
                <Lat>43.508</Lat>
                <Lon>16.426</Lon>
                <Podatci>
                    <Temp>10.0</Temp>
                    <Vlaga>84</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.5</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Makarska</GradIme>
                <Lat>43.288</Lat>
                <Lon>17.020</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Jure-Biokovo</GradIme>
                <Lat>43.342</Lat>
                <Lon>17.053</Lon>
                <Podatci>
                    <Temp>3.0</Temp>
                    <Vlaga>26</Vlaga>
                    <Tlak>835.6*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>5.2</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Hvar</GradIme>
                <Lat>43.171</Lat>
                <Lon>16.437</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Komiža</GradIme>
                <Lat>43.048</Lat>
                <Lon>16.085</Lon>
                <Podatci>
                    <Temp>11.8</Temp>
                    <Vlaga>80</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ploče</GradIme>
                <Lat>43.048</Lat>
                <Lon>17.443</Lon>
                <Podatci>
                    <Temp>5.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Dubrovnik</GradIme>
                <Lat>42.645</Lat>
                <Lon>18.085</Lon>
                <Podatci>
                    <Temp>9.2</Temp>
                    <Vlaga>73</Vlaga>
                    <Tlak>1031.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Prevlaka</GradIme>
                <Lat>42.393</Lat>
                <Lon>18.531</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>4.5</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Lastovo</GradIme>
                <Lat>42.768</Lat>
                <Lon>16.900</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.2</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>8.0</VjetarBrzina>
                    <Vrijeme>umjereno jak vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Palagruža</GradIme>
                <Lat>42.393</Lat>
                <Lon>16.255</Lon>
                <Podatci>
                    <Temp>11.5</Temp>
                    <Vlaga>75</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>5.9</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
        </Hrvatska>';
        DB::connection('sqlite')->table('xmls')->insert([
            'source_id' => 1, // 'ACTR',
            'content' => $content3,
            'md5' => md5($content3),
            'sha1' => sha1($content3),
            'loaded' => '0',
            'created_at' => date('Y-m-d H:i:s', mktime('21','00','00','01','28','2018')),
            'updated_at' => date('Y-m-d H:i:s', mktime('21','00','00','01','28','2018'))
        ]);
        Log::debug('Done Seeder: ProdSeedMissingXmls: ACTR, no: ' . $no);
        $no++;

        $content4 = '<?xml version="1.0" encoding="UTF-8"?>
        <Hrvatska>
            <DatumTermin>
                <Datum>28.01.2018</Datum>
                <Termin>21</Termin>
            </DatumTermin>
            <Grad autom="0">
                <GradIme>RC Bilogora</GradIme>
                <Lat>45.884</Lat>
                <Lon>17.200</Lon>
                <Podatci>
                    <Temp>1.8</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.8</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>2.1</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Bjelovar</GradIme>
                <Lat>45.910</Lat>
                <Lon>16.869</Lon>
                <Podatci>
                    <Temp>3.5</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Crikvenica</GradIme>
                <Lat>45.173</Lat>
                <Lon>14.689</Lon>
                <Podatci>
                    <Temp>7.2</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>0.2</VjetarBrzina>
                    <Vrijeme>-</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Daruvar</GradIme>
                <Lat>45.592</Lat>
                <Lon>17.210</Lon>
                <Podatci>
                    <Temp>2.0</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.0</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Dubrovnik</GradIme>
                <Lat>42.645</Lat>
                <Lon>18.085</Lon>
                <Podatci>
                    <Temp>9.2</Temp>
                    <Vlaga>73</Vlaga>
                    <Tlak>1031.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.2</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Gospić</GradIme>
                <Lat>44.551</Lat>
                <Lon>15.373</Lon>
                <Podatci>
                    <Temp>1.3</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1035.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gorice (kod Nove Gradiške)</GradIme>
                <Lat>45.224</Lat>
                <Lon>17.278</Lon>
                <Podatci>
                    <Temp>2.2</Temp>
                    <Vlaga>96</Vlaga>
                    <Tlak>1033.9</Tlak>
                    <TlakTend>-0.6</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Gradište (kod Županje)</GradIme>
                <Lat>45.159</Lat>
                <Lon>18.704</Lon>
                <Podatci>
                    <Temp>2.7</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Hvar</GradIme>
                <Lat>43.171</Lat>
                <Lon>16.437</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>2.4</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Karlovac</GradIme>
                <Lat>45.494</Lat>
                <Lon>15.565</Lon>
                <Podatci>
                    <Temp>0.1</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Knin</GradIme>
                <Lat>44.041</Lat>
                <Lon>16.207</Lon>
                <Podatci>
                    <Temp>5.5</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1033.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Komiža</GradIme>
                <Lat>43.048</Lat>
                <Lon>16.085</Lon>
                <Podatci>
                    <Temp>11.8</Temp>
                    <Vlaga>80</Vlaga>
                    <Tlak>1033.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Krapina</GradIme>
                <Lat>46.138</Lat>
                <Lon>15.888</Lon>
                <Podatci>
                    <Temp>3.4</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Križevci</GradIme>
                <Lat>46.029</Lat>
                <Lon>16.554</Lon>
                <Podatci>
                    <Temp>3.4</Temp>
                    <Vlaga>94</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Kutjevo</GradIme>
                <Lat>45.424</Lat>
                <Lon>17.876</Lon>
                <Podatci>
                    <Temp>0.7</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1036.5</Tlak>
                    <TlakTend>-0.3</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Lastovo</GradIme>
                <Lat>42.768</Lat>
                <Lon>16.900</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.2</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>8.0</VjetarBrzina>
                    <Vrijeme>umjereno jak vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Malinska</GradIme>
                <Lat>45.126</Lat>
                <Lon>14.527</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>88</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Makarska</GradIme>
                <Lat>43.288</Lat>
                <Lon>17.020</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>0.8</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Mali Lošinj</GradIme>
                <Lat>44.533</Lat>
                <Lon>14.472</Lon>
                <Podatci>
                    <Temp>8.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2</VjetarBrzina>
                    <Vrijeme>umjereno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>RC Monte Kope</GradIme>
                <Lat>44.812</Lat>
                <Lon>13.873</Lon>
                <Podatci>
                    <Temp>9.4</Temp>
                    <Vlaga>86</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>2.0</VjetarBrzina>
                    <Vrijeme>povjetarac</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ogulin</GradIme>
                <Lat>45.263</Lat>
                <Lon>15.222</Lon>
                <Podatci>
                    <Temp>0.8</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>0.1</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Opatija</GradIme>
                <Lat>45.341</Lat>
                <Lon>14.313</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1031.9</Tlak>
                    <TlakTend>-0.2</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Osijek-Čepin</GradIme>
                <Lat>45.503</Lat>
                <Lon>18.561</Lon>
                <Podatci>
                    <Temp>2.4</Temp>
                    <Vlaga>100</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>-0.4</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2.3</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Palagruža</GradIme>
                <Lat>42.393</Lat>
                <Lon>16.255</Lon>
                <Podatci>
                    <Temp>11.5</Temp>
                    <Vlaga>75</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>0.0</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>5.9</VjetarBrzina>
                    <Vrijeme>umjeren vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Parg-Čabar</GradIme>
                <Lat>45.594</Lat>
                <Lon>14.631</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>930.7*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.9</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Pazin</GradIme>
                <Lat>45.241</Lat>
                <Lon>13.945</Lon>
                <Podatci>
                    <Temp>4.9</Temp>
                    <Vlaga>99</Vlaga>
                    <Tlak>1034.4</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>0.4</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>NP Plitvička jezera</GradIme>
                <Lat>44.881</Lat>
                <Lon>15.620</Lon>
                <Podatci>
                    <Temp>3.0</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1034.9</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>E</VjetarSmjer>
                    <VjetarBrzina>0.3</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Ploče</GradIme>
                <Lat>43.048</Lat>
                <Lon>17.443</Lon>
                <Podatci>
                    <Temp>5.8</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1033.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Porer - svjetionik</GradIme>
                <Lat>44.758</Lat>
                <Lon>13.890</Lon>
                <Podatci>
                    <Temp>9.8</Temp>
                    <Vlaga>83</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.4</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Prevlaka</GradIme>
                <Lat>42.393</Lat>
                <Lon>18.531</Lon>
                <Podatci>
                    <Temp>7.7</Temp>
                    <Vlaga>92</Vlaga>
                    <Tlak>1033.4</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>N</VjetarSmjer>
                    <VjetarBrzina>4.5</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>RC Puntijarka</GradIme>
                <Lat>45.908</Lat>
                <Lon>15.968</Lon>
                <Podatci>
                    <Temp>5.2</Temp>
                    <Vlaga>80</Vlaga>
                    <Tlak>915.5*</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>3.2</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rab</GradIme>
                <Lat>44.756</Lat>
                <Lon>14.769</Lon>
                <Podatci>
                    <Temp>8.0</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1033.5</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>C</VjetarSmjer>
                    <VjetarBrzina>0.0</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Rijeka</GradIme>
                <Lat>45.337</Lat>
                <Lon>14.443</Lon>
                <Podatci>
                    <Temp>7.4</Temp>
                    <Vlaga>90</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.3</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Ivan na pučini - svjetionik</GradIme>
                <Lat>45.043</Lat>
                <Lon>13.614</Lon>
                <Podatci>
                    <Temp>9.6</Temp>
                    <Vlaga>98</Vlaga>
                    <Tlak>1034.7</Tlak>
                    <TlakTend>-0.1</TlakTend>
                    <VjetarSmjer>SW</VjetarSmjer>
                    <VjetarBrzina>0.6</VjetarBrzina>
                    <Vrijeme>lahor</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Senj</GradIme>
                <Lat>44.993</Lat>
                <Lon>14.903</Lon>
                <Podatci>
                    <Temp>8.5</Temp>
                    <Vlaga>77</Vlaga>
                    <Tlak>1033.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.7</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Sisak</GradIme>
                <Lat>45.499</Lat>
                <Lon>16.367</Lon>
                <Podatci>
                    <Temp>4.4</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>0.5</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Slavonski Brod</GradIme>
                <Lat>45.159</Lat>
                <Lon>17.995</Lon>
                <Podatci>
                    <Temp>2.6</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1034.1</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.4</VjetarBrzina>
                    <Vrijeme>magla</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Split-Marjan</GradIme>
                <Lat>43.508</Lat>
                <Lon>16.426</Lon>
                <Podatci>
                    <Temp>10.0</Temp>
                    <Vlaga>84</Vlaga>
                    <Tlak>1032.9</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>1.5</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Sv.Jure-Biokovo</GradIme>
                <Lat>43.342</Lat>
                <Lon>17.053</Lon>
                <Podatci>
                    <Temp>3.0</Temp>
                    <Vlaga>26</Vlaga>
                    <Tlak>835.6*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NE</VjetarSmjer>
                    <VjetarBrzina>5.2</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Šibenik</GradIme>
                <Lat>43.728</Lat>
                <Lon>15.906</Lon>
                <Podatci>
                    <Temp>8.0</Temp>
                    <Vlaga>97</Vlaga>
                    <Tlak>1033.2</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Varaždin</GradIme>
                <Lat>46.283</Lat>
                <Lon>16.364</Lon>
                <Podatci>
                    <Temp>2.9</Temp>
                    <Vlaga>95</Vlaga>
                    <Tlak>1033.6</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>S</VjetarSmjer>
                    <VjetarBrzina>2.4</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="1">
                <GradIme>Veli Rat - svjetionik</GradIme>
                <Lat>44.152</Lat>
                <Lon>14.820</Lon>
                <Podatci>
                    <Temp>10.5</Temp>
                    <Vlaga>85</Vlaga>
                    <Tlak>1034.3</Tlak>
                    <TlakTend>+0.1</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>3.8</VjetarBrzina>
                    <Vrijeme>slab vjetar</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zadar</GradIme>
                <Lat>44.130</Lat>
                <Lon>15.206</Lon>
                <Podatci>
                    <Temp>10.6</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.8</VjetarBrzina>
                    <Vrijeme>pretežno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Grič</GradIme>
                <Lat>45.814</Lat>
                <Lon>15.972</Lon>
                <Podatci>
                    <Temp>4.2</Temp>
                    <Vlaga>89</Vlaga>
                    <Tlak>1033.8</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>W</VjetarSmjer>
                    <VjetarBrzina>1.6</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zagreb-Maksimir</GradIme>
                <Lat>45.822</Lat>
                <Lon>16.034</Lon>
                <Podatci>
                    <Temp>3.9</Temp>
                    <Vlaga>93</Vlaga>
                    <Tlak>1034.0</Tlak>
                    <TlakTend>-0.3</TlakTend>
                    <VjetarSmjer>SE</VjetarSmjer>
                    <VjetarBrzina>1.1</VjetarBrzina>
                    <Vrijeme>potpuno oblačno</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
            <Grad autom="0">
                <GradIme>Zavižan</GradIme>
                <Lat>44.815</Lat>
                <Lon>14.975</Lon>
                <Podatci>
                    <Temp>4.8</Temp>
                    <Vlaga>38</Vlaga>
                    <Tlak>851.3*</Tlak>
                    <TlakTend>-</TlakTend>
                    <VjetarSmjer>NW</VjetarSmjer>
                    <VjetarBrzina>1.7</VjetarBrzina>
                    <Vrijeme>pretežno vedro</Vrijeme>
                    <VrijemeZnak>-</VrijemeZnak>
                </Podatci>
            </Grad>
        </Hrvatska>';
        DB::connection('sqlite')->table('xmls')->insert([
            'source_id' => 2, // 'ACTA',
            'content' => $content4,
            'md5' => md5($content4),
            'sha1' => sha1($content4),
            'loaded' => '0',
            'created_at' => date('Y-m-d H:i:s', mktime('21','00','00','01','28','2018')),
            'updated_at' => date('Y-m-d H:i:s', mktime('21','00','00','01','28','2018'))
        ]);
        Log::debug('Done Seeder: ProdSeedMissingXmls: ACTA, no: ' . $no);

    }
}
