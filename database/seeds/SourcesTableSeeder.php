<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->insert([
            'code' => 'ACTR',
            'desc' => 'Vrijeme u Hrvatskoj (regije)',
            'url' => 'http://vrijeme.hr/hrvatska_n.xml',
            'refresh' => 'hourly',
        ]);
        DB::table('sources')->insert([
            'code' => 'ACTA',
            'desc' => 'Vrijeme u Hrvatskoj (abecedno)',
            'url' => 'http://vrijeme.hr/hrvatska1_n.xml',
            'refresh' => 'hourly',
        ]);
        DB::table('sources')->insert([
            'code' => 'ACTEU',
            'desc' => 'Vrijeme u Europi',
            'url' => 'http://vrijeme.hr/europa_n.xml',
            'refresh' => 'hourly',
        ]);
        DB::table('sources')->insert([
            'code' => 'SEA',
            'desc' => 'Temperature mora Jadran',
            'url' => 'http://vrijeme.hr/more_n.xml',
            'refresh' => 'hourly, between(8, 17)',
        ]);
        DB::table('sources')->insert([
            'code' => 'RAIN',
            'desc' => 'Količina oborine',
            'url' => 'http://vrijeme.hr/oborina.xml',
            'refresh' => 'twice daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'SNW',
            'desc' => 'Visine snijega',
            'url' => 'http://vrijeme.hr/snijeg_n.xml',
            'refresh' => 'twice daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'UVI',
            'desc' => 'UV indeks',
            'url' => 'http://vrijeme.hr/uvi.xml',
            'refresh' => 'hourly, between(8, 17)',
        ]);
        
        DB::table('sources')->insert([
            'code' => 'HRD',
            'desc' => 'Hrvatska danas',
            'url' => 'http://prognoza.hr/hrdanas_n.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'HRS',
            'desc' => 'Hrvatska sutra',
            'url' => 'http://prognoza.hr/hrsutra_n.xml',
            'refresh' => 'daily',
        ]);

        DB::table('sources')->insert([
            'code' => 'ZGD',
            'desc' => 'Zagreb danas',
            'url' => 'http://prognoza.hr/zgdanas_n.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'ZGS',
            'desc' => 'Zagreb sutra',
            'url' => 'http://prognoza.hr/zgsutra_n.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'PR3',
            'desc' => 'Izgledi vremena za 3 dana',
            'url' => 'http://prognoza.hr/izgledi.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'PRJ',
            'desc' => 'Prognoze za Jadran',
            'url' => 'http://prognoza.hr/jadran_h.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'PRP',
            'desc' => 'Prognoze za pomorce',
            'url' => 'http://prognoza.hr/pomorci.xml',
            'refresh' => 'daily',
        ]);

        DB::table('sources')->insert([
            'code' => 'BIO',
            'desc' => 'Biometeorološka prognoza',
            'url' => 'http://vrijeme.hr/bio/bio.xml',
            'refresh' => 'daily',
        ]);
        DB::table('sources')->insert([
            'code' => 'TPL',
            'desc' => 'Toplinski valovi',
            'url' => 'http://vrijeme.hr/toplinskival_5.xml',
            'refresh' => 'daily',
        ]);
    }
}
