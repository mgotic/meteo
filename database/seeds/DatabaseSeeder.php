<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed --class=UserTableSeeder
        // $this->call(UsersTableSeeder::class);
        // $this->call(SourcesTableSeeder::class);
        // run once to fill tables
        // $this->call(ProdSeedMissingXmls::class);
        // $this->call(StationTypesSeeder::class);
    }
}
