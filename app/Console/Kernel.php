<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('meteo:getUrl ACTR')->cron('2,7,16,35,55 * * * * *');
        $schedule->command('meteo:getUrl ACTA')->cron('2,7,16,35,55 * * * * *');
        $schedule->command('meteo:getUrl ACTEU')->cron('3,9,25,50 * * * * *');
        $schedule->command('meteo:getUrl SEA')->cron('5,15,45 * * * * *');
        $schedule->command('meteo:getUrl RAIN')->cron('20 * * * * *');
        $schedule->command('meteo:getUrl SNW')->cron('25 * * * * *');
        $schedule->command('meteo:getUrl UVI')->cron('34 * * * * *');
        $schedule->command('meteo:getUrl HRD')->cron('10 * * * * *');
        $schedule->command('meteo:getUrl HRS')->cron('11 * * * * *');
        $schedule->command('meteo:getUrl ZGD')->cron('12 * * * * *');
        $schedule->command('meteo:getUrl ZGS')->cron('13 * * * * *');
        $schedule->command('meteo:getUrl PR3')->cron('24 * * * * *');
        $schedule->command('meteo:getUrl PRJ')->cron('25 * * * * *');
        $schedule->command('meteo:getUrl PRP')->cron('26 * * * * *');
        $schedule->command('meteo:getUrl BIO')->cron('27 * * * * *');
        $schedule->command('meteo:getUrl TPL')->cron('28 * * * * *');
        // nedostaje ih još par...
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
