<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Models\Location;
use App\Models\Precipitation;
use App\Models\Xml;
use App\Models\Source;

class loadCurrentRainXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:loadRain {id?} {number?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LoadXML to DB (rain data), supply xml_id, stats (s/S), latest (l/L) and number or all (a/A) unloaded for current weather xml type (default L 100)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $number = $this-> argument('number');

        $starting = Carbon::now('Europe/Zagreb');
        $this->info('Starting: loadCurrentRainXML for id: ' . $id . ' at: ' . $starting->toDateTimeString());
        Log::debug('Starting: loadCurrentRainXML for id: ' . $id . ' at: ' . $starting->toDateTimeString());
        
        // get the values from xml
        $source_code = 'RAIN';
        $code = Source::where('code', '=', $source_code)->first();

        $take = 100;
       
        if(isset($id)) {
            if(is_numeric($id)) {
                $raw_data = Xml::where('source_id','=', $code->id)->where('xml_id', '=', $id)->get();
            }   else    {
                    if(strtolower($id)== 's')    {
                        $left = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->count();
                        $done = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '1')->count();
                        $failed = Xml::where('source_id','=', $code->id)->where('loaded', '<', '0')->count();
                        $total = $left + $done + $failed;
                        $this->line('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                        Log::debug('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                        exit();  
                    }
                    if(strtolower($id)== 'l')    {
                        if(isset($number))  {
                            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->take($number)->get();
                        }   else    {
                            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->take($take)->get();
                        }
                    }   else    {
                        $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->get();
                    }
            }
        } else {
            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->take($take)->get();
        }

        $raw_data_count = $raw_data->count();
        $counter = 1;

        // loop  content, loaded
        foreach($raw_data as $data)
        {
            $id = $data->id;
            $content = $data->content;
            $elapsed = Carbon::now('Europe/Zagreb')->diffInSeconds($starting);     
            $this->line('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');
            Log::debug('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');

            try {   
                $xml_content = simplexml_load_string($content);

                $datum = Carbon::createFromFormat('d.m.Y.', $xml_content->datumtermin->datum, 'Europe/Zagreb')->toDateString();
                $sat = Carbon::createFromFormat('H', (string) $xml_content->datumtermin->termin, 'Europe/Zagreb')->toTimeString();

                foreach($xml_content->grad as $grad)
                {
                    // getOrCreateLocation($name, $gps = null, $masl = null, $country = 'HRV', $create = true)
                    $location_id = Location::getOrCreateLocation($grad->ime, null, null, 'HRV', true)->id;
                    
                    if(is_float((float)$grad->kolicina))
                    {
                        $precipitation_observed = (float)$grad->kolicina;
                    }   else {
                        Log::warning('loadCurrentRainXML -> $precipitation_observed is not float value!');
                    }

                    Log::debug('Prepared data for precipitation: ' . $datum . '/' . $sat  . '/' . $location_id  . '/' . $precipitation_observed . '.');

                    // kreiranja novih objekata
                    $data_list = [
                        'precipitation_date' => $datum,
                        'precipitation_time' => $sat,
                        'location_id' => $location_id,
                        'precipitation_observed' => $precipitation_observed
                    ];

                    $adding = Precipitation::addPrecipitationData($data_list);
                    Log::debug('model Weather added: ' . $adding); 
                }
                // set xml as loaded
                $load = Xml::setXmlLoaded($id, 1, true);
                Log::debug('XML file load done, xml status updated: ' . $load);

            } catch (\Exception $e) {
                $this->error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                Log::error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                // loaded to -1 for error
                $load = Xml::setXmlLoaded($id, -1, false);
                Log::debug('XML file load failed, xml status updated: ' . $load);
            }
            $counter = $counter+1;
        }
        $this->info('Ended: loadCurrentRainXML at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting));
        Log::debug('Ended: loadCurrentRainXML at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting));
    }
}
