<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use GuzzleHttp\Client;

use App\Models\Source;
use App\Models\Xml;

class GetUrlForCode extends Command
{
    //The name and signature of the console command.
    protected $signature = 'meteo:getUrl {code}';

    //The console command description.
    protected $description = 'GetUrl for code';

    //Create a new command instance.
    public function __construct()
    {
        parent::__construct();
    }

    // Execute the console command.
    public function handle()
    {
        $code = $this->argument('code');
        $this->line('Starting: GetUrl for code: ' . $code);
        Log::debug('Starting: GetUrl for code: ' . $code);

        //get url for code
        //if fails...
        try {
            $url = Source::where('code', 'like', $code)->firstOrFail();
            $this->line('Url is: ' . $url->url);
            Log::debug('Url is: ' . $url->url);
        } catch (\Exception $e) {
            $this->error('Failed to retriveve url for code: ' . $code . ', with error: ' . $e->getMessage());
            Log::error('Failed to retriveve url for code: ' . $code . ', with error: ' . $e->getMessage());
            exit();
        }
        // get source
        try {
            $client = new Client();
            $response = $client->get($url->url, ['verify' => false]);
            if($response->getStatusCode() <> 200)    {
                $this->error('Failed to get url for code (!=200): ' . $url->code . ', url: ' . $url->url . ', with code/error: ' . $response->getStatusCode() . '/' . $response->getReasonPhrase());
                Log::error('Failed to get url for code (!=200): ' . $url->code . ', url: ' . $url->url . ', with code/error: ' . $response->getStatusCode() . '/' . $response->getReasonPhrase());
                exit();
            }
            else {
                $xml = (string) $response->getBody();
                $this->line('Got the body...');
                Log::debug('Got the body...');
            }            
        }   catch (\Exception $e)   {
            $this->error('Failed to get url for code: ' . $url->url . ', with error: ' . $e->getMessage());
            Log::error('Failed to get url for code: ' . $url->url . ', with error: ' . $e->getMessage());
            exit();
        }

        $md5 = md5($xml);
        $sha1 = sha1($xml);
        $source_id = intval($url->id);
        $this->line('Check if record allready exists...: ' .  $source_id . '/' . $md5 . '/' . $sha1);
        Log::debug('Check if record allready exists...: ' . $source_id . '/' . $md5 . '/' . $sha1);

        $exister = Xml::where('source_id','=', $source_id)->where('md5', '=', $md5)->where('sha1', '=', $sha1)->count();
        Log::debug('Does data with same md5/sha1 exists - count: ' . $exister);
        
        if($exister == 0)  {
            $this->line('Check if record allready exists... Does not. Trying to add one.');
            Log::debug('Check if record allready exists... Does not. Trying to add one.');
            try {
                $record = new Xml;
                $record->source_id = $source_id;
                $record->content = $xml;
                $record->md5 = $md5;
                $record->sha1 = $sha1;
                // store to db
                $record->save();
                $this->line('Saved the record: ' . $source_id . '/' . $md5 . '/' . $sha1);
                Log::info('Saved the record: ' . $source_id . '/' . $md5 . '/' . $sha1);
            } catch (\Exception $e)   {
                $this->error('Failed to add new record: ' . $source_id . '/' . $md5 . '/' . $sha1 . ', with error: ' . $e->getMessage());
                Log::error('Failed to add new record: ' . $source_id . '/' . $md5 . '/' . $sha1 . ', with error: ' . $e->getMessage());
                exit();
            }
        }
        else    {
            $this->line('Check if record allready exists... It does, continuing...');
            Log::debug('Check if record allready exists... It does, continuing...');
        }
        $this->line('Ended: GetUrl for code: ' . $code);
        Log::debug('Ended: GetUrl for code: ' . $code);  
    }
}
