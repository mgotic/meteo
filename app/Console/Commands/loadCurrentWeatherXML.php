<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Models\Xml;
use App\Models\Weather;
use App\Models\Source;
use App\Models\Location;
use App\Models\Station_Type;
use App\Models\Weather_Condition;

class loadCurrentWeatherXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:loadWeather {source_code} {action} {number?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LoadXML to DB (current weather), supply: source_code (ACTA/ACTR/ACTEU), action (a/l/s) with amount (up to 5000 for l action)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $source_code = strtoupper($this->argument('source_code'));
        $action = strtolower($this->argument('action'));
        $number = $this-> argument('number');

        $starting = Carbon::now('Europe/Zagreb');
        $this->line('Starting: loadCurrentWeatherXML for source_code: ' . $source_code . ', action: ' . $action . ', and number: ' . $number . ' at: ' . $starting->toDateTimeString());
        Log::debug('Starting: loadCurrentWeatherXML for source_code: ' . $source_code . ', action: ' . $action . ', and number: ' . $number . ' at: ' . $starting->toDateTimeString());
        
        if(!in_array($source_code, ['ACTA','ACTR','ACTEU'])) {
            $this->error('Error: loadCurrentWeatherXML, unknown source_code: ' . $source_code . '.');
            Log::error('Error: loadCurrentWeatherXML, unknown source_code: ' . $source_code . '.');    
            exit();
        }

        if(!in_array($action, ['a','l','s'])) {
            $this->error('Error: loadCurrentWeatherXML, unknown action: ' . $action . '.');
            Log::error('Error: loadCurrentWeatherXML, unknown action: ' . $action . '.');    
            exit();
        }

        $source_id = Source::where('code', '=', $source_code)->first()->id;
        $take = 100; // default for l
        
        switch($action) {
            case 'a':
            $raw_data = Xml::where('source_id','=', $source_id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->get();
                break;
            case 'l':
                if(isset($number))  {
                    $raw_data = Xml::where('source_id','=', $source_id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->take($number)->get();
                }   else    {
                    $raw_data = Xml::where('source_id','=', $source_id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->take($take)->get();
                }
                break;
            case 's':
                $left = Xml::where('source_id','=', $source_id)->where('loaded', 'like', '0')->count();
                $done = Xml::where('source_id','=', $source_id)->where('loaded', 'like', '1')->count();
                $failed = Xml::where('source_id','=', $source_id)->where('loaded', '<', '0')->count();
                $total = $left + $done + $failed;
                
                $this->line('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                Log::debug('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                exit();
        }

        $raw_data_count = $raw_data->count();
        $counter = 1;

        foreach($raw_data as $data)
        {
            $id = $data->id;
            $content = $data->content;
            $elapsed = Carbon::now('Europe/Zagreb')->diffInSeconds($starting);     
            $this->line('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');
            Log::debug('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');

            // parsiranje xml-a
           
            try {   
                $xml_content = simplexml_load_string($content);
                $datum = Carbon::createFromFormat('d.m.Y', (string)$xml_content->DatumTermin->Datum, 'Europe/Zagreb')->toDateString();
                $sat = Carbon::createFromFormat('H', (string) $xml_content->DatumTermin->Termin, 'Europe/Zagreb')->toTimeString();

                foreach($xml_content->Grad as $grad)    {
                    if($grad->Lat and $grad->Lon)   {
                        $gps = $grad->Lat . ',' . $grad->Lon;
                    }   else    {
                        $gps = null;
                    }
                    in_array($source_code, ['ACTA','ACTR']) ? $country = 'HRV' : $country='XXX';
                    $location_id = Location::getOrCreateLocation($grad->GradIme, $gps, null, $country, true)->id;

                    if(is_integer((integer)$grad['autom'])) {
                        $station_type_id = Station_type::getStationType((string)$grad['autom']);
                    } else {
                        $station_type_id = null;
                    }
                    if(is_string((string)$grad->Podatci->VjetarSmjer))  {
                        $wind_direction = (string)$grad->Podatci->VjetarSmjer;
                    } else {
                        $wind_direction = null;
                    }
                    if(is_float((float)$grad->Podatci->VjetarBrzina))   {
                        $wind_speed = (float)$grad->Podatci->VjetarBrzina;
                    }   else {
                        $wind_speed = null;
                    }
                    
                    if(is_float((float)$grad->Podatci->Temp))   {
                        $air_temperature = (float)$grad->Podatci->Temp;
                    }   else    {
                        $air_temperature = null;
                    }

                    if(is_integer((integer)$grad->Podatci->Vlaga))  {
                        $relative_humidity = (integer)$grad->Podatci->Vlaga;
                    } else  {
                        $relative_humidity = null;
                    }

                    if(is_float((float)$grad->Podatci->Tlak))   {
                        $atmospheric_pressure = (float)$grad->Podatci->Tlak;
                    } else  {
                        $atmospheric_pressure = null;
                    }

                    if(is_float((float)$grad->Podatci->TlakTend))   {
                        $pressure_tendency = (float)$grad->Podatci->TlakTend;
                    }   else    {
                        $pressure_tendency = null;
                    }
                    
                    if(is_string((string)$grad->Podatci->Vrijeme))  {
                        $weather_condition_id = Weather_Condition::getWeatherCondition((string)$grad->Podatci->Vrijeme)->id;
                    } else {
                        $weather_condition_id = null;
                    }

                    if(is_string((string)$grad->Podatci->VrijemeZnak))  {
                        $weather_icon = (string)$grad->Podatci->VrijemeZnak;
                    } else  {
                        $weather_icon = null;
                    }

                    Log::debug('Prepared data for current weather: ' . $location_id . '/' . $station_type_id  . '/' . $wind_direction
                    . '/' . $wind_speed . '/' . $air_temperature . '/' . $relative_humidity . '/' . $atmospheric_pressure
                    . '/' . $pressure_tendency . '/' . $weather_condition_id . '/' . $weather_icon . '.');
                    
                    // kreiranja novih objekata

                    $data_list = [
                        'weather_date' => $datum,
                        'weather_time' => $sat,
                        'location_id' => $location_id,
                        'station_type_id' => $station_type_id,
                        'wind_direction' => $wind_direction,
                        'wind_speed' => $wind_speed,
                        'air_temperature' => $air_temperature,
                        'relative_humidity' => $relative_humidity,
                        'atmospheric_pressure' => $atmospheric_pressure,
                        'pressure_tendency' => $pressure_tendency,
                        'weather_condition_id' => $weather_condition_id,
                        'weather_icon' => $weather_icon
                    ];
                    $adding = Weather::addWeatherData($data_list);
                    Log::debug('model Weather added: ' . $adding); 
                }
                // set xml as loaded
                $load = Xml::setXmlLoaded($id, 1, true);
                Log::debug('XML file load done, xml status updated: ' . $load);

            } catch (\Exception $e) {
                $this->error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                Log::error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                // loaded to -1 for error
                $load = Xml::setXmlLoaded($id, -1, false);
                Log::debug('XML file load failed, xml status updated: ' . $load);
            }
            $counter = $counter+1;
        } 

    $this->line('Ended: loadCurrentWeatherXML at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting));
    Log::debug('Ended: loadCurrentWeatherXML at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting)); 

    }
}
