<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

use App\Models\Source;
use App\Models\Xml;
use App\Models\Forecast;

class loadForecastXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meteo:loadForecast {type} {id?} {number?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'LoadXML to DB (forcast data), supply type (forecast type - mandatory: HrD, HrS, ZgD, ZgS) and xml_id or stats (s/S), latest (l/L) and number or all (a/A) unloaded for current weather xml type (default L 100)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $source_code = strtoupper($this->argument('type'));
        $id = $this->argument('id');
        $number = $this-> argument('number');

        $starting = Carbon::now('Europe/Zagreb');
        $this->line('Starting: loadForecast for id: ' . $id . ', and number: ' . $number . ' at: ' . $starting->toDateTimeString());
        Log::debug('Starting: loadForecast for id: ' . $id . ', and number: ' . $number . ' at: ' . $starting->toDateTimeString());
        
        // available forecast codes: HrD, HrS, ZgD, ZgS
        $code = Source::whereIn($source_code, ['ZGD','ZGS','HRD','HRS'])->where('code', '=', $source_code)->firstOrFail();       
        $take = 100;

        if(isset($id)) {
            if(is_numeric($id)) {
                $raw_data = Xml::where('source_id','=', $code->id)->where('xml_id', '=', $id)->orderBy('id', 'asc')->get();
            }   else    {
                    if(strtolower($id)== 's')    {
                        $left = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->count();
                        $done = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '1')->count();
                        $failed = Xml::where('source_id','=', $code->id)->where('loaded', '<', '0')->count();
                        $total = $left + $done + $failed;
                        $this->line('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                        Log::debug('Stats: loaded/left/failed/total: ' . $done . '/' . $left . '/' . $failed . '/' . $total . ', percentage: ' . round($done/$total*100) . '/' . round($left/$total*100) . '/' . round($failed/$total*100) . ' (%)');
                        exit();  
                    }
                    if(strtolower($id)== 'l')    {
                        if(isset($number))  {
                            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->take($number)->get();
                        }   else    {
                            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->take($take)->get();
                        }
                    }   else    {
                        $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->orderBy('id', 'asc')->get();
                    }
            }
        } else {
            $raw_data = Xml::where('source_id','=', $code->id)->where('loaded', 'like', '0')->take($take)->orderBy('id', 'asc')->get();
        }

        $raw_data_count = $raw_data->count();
        $counter = 1;

        // loop  content, loaded
        foreach($raw_data as $data)
        {
            $id = $data->id;
            $content = $data->content;
            $elapsed = Carbon::now('Europe/Zagreb')->diffInSeconds($starting);     
            $this->line('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');
            Log::debug('Stats: ' . $counter . '/' . $raw_data_count . ' (' . round($counter/$raw_data_count*100, 2) . '%), elapsed seconds: ' . $elapsed . ', id is ' . $id . '. ');

            try {   
                $xml_content = simplexml_load_string($content);
                // customize...
    // dd($xml_content);
                $forecast_type_id = $data->source_id;
                $naslov = $xml_content->Naslov;
                $tekst = $xml_content->Tekst;
                $nastao = $xml_content->Nastao;

                $tmp = explode(" ", $naslov);
                if(strtolower($tmp[3]) == 'hrvatsku')   {
                    $forecest_for = Carbon::createFromFormat('d.m.Y', (string)$tmp[5], 'Europe/Zagreb')->toDateString();
                } else  {
                    $forecest_for = Carbon::createFromFormat('d.m.Y', (string)$tmp[7], 'Europe/Zagreb')->toDateString();
                }
                // $tmp može imati - i . kao delimiter u datumu, ako je - onda je y.m.d, inače d.m.y

                $tmp2 = explode(" ", $nastao);
                $tmp3 = explode(".", $tmp2[2]);
                if(count($tmp3) < 2)    {
                    $tmp4 = explode("-", $tmp2[2]);
                    $forecast_from = Carbon::create($tmp4[0], $tmp4[1], $tmp4[2], $tmp2[4], 0, 0, 'Europe/Zagreb')->toDateTimeString();                    
                }   else    {
                    $forecast_from = Carbon::create($tmp3[2], $tmp3[1], $tmp3[0], $tmp2[4], 0, 0, 'Europe/Zagreb')->toDateTimeString();
                }
                

                // kreiranja novih objekata
                $data_list = [
                    'forecast_type_id' => $forecast_type_id,
                    'naslov' => (string)$naslov,
                    'tekst' => trim((string)$tekst),
                    'nastao' => (string)$nastao,
                    'forecest_for' => $forecest_for,
                    'forecest_from' => $forecast_from
                ];

                $adding = Forecast::addForecastData($data_list);
                Log::debug('model Foreacast added: ' . $adding);

                // set xml as loaded
                $load = Xml::setXmlLoaded($id, 1, true);
                Log::debug('XML file load done, xml status updated: ' . $load);

            } catch (\Exception $e) {
                
                $this->error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                Log::error('Failed to load xml: ' . $id . ', with error: ' . $e->getMessage());
                // loaded to -1 for error
                $load = Xml::setXmlLoaded($id, -1, false);
                Log::debug('XML file load failed, xml status updated: ' . $load);
            }
            $counter = $counter+1;
        }
    
    $this->line('Ended: loadForecastType at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting));
    Log::debug('Ended: loadForecastType at: ' . Carbon::now('Europe/Zagreb')->toDateTimeString() . ', elapsed seconds: ' . Carbon::now('Europe/Zagreb')->diffInSeconds($starting)); 

    }
}
