<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Weather_Condition extends Model
{
    protected $fillable = ['desc','descsc'];
    protected $connection = 'sqlite_data';
    protected $table = 'weather_conditions';
    
    public static function getWeatherCondition($input)
    {        
        $conditionsc = strtolower($input);
        $conditionsc = preg_replace("/[^a-z0-9_\s-]/", "", $conditionsc);
        $conditionsc = preg_replace("/[\s-]+/", " ", $conditionsc);
        $conditionsc = preg_replace("/[\s_]/", "-", $conditionsc);

        $condition = Weather_Condition::firstOrCreate(
                ['descsc' => $conditionsc],
                ['descsc' => $conditionsc, 'desc' => $input]
        );
        Log::debug('getWeatherCondition function in model, input: ' . $input . ', output: ' . $condition->id . '.');
        return $condition;
    }
}
