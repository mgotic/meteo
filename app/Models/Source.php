<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $fillable = ['code','desc','url','refresh'];
    protected $connection = 'sqlite';

}
