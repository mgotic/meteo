<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Precipitation extends Model
{
    protected $fillable=['precipitation_date','precipitation_time','location_id','precipitation_observed'];
    protected $connection = 'sqlite_data';

    public static function addPrecipitationData($input)
    {
        $precipitation = Precipitation::firstOrCreate([
            'precipitation_date' => $input['precipitation_date'], 
            'precipitation_time' => $input['precipitation_time'], 
            'location_id' => $input['location_id']
        ],[
            'precipitation_date' => $input['precipitation_date'], 
            'precipitation_time' => $input['precipitation_time'], 
            'location_id' => $input['location_id'],
            'precipitation_observed' => $input['precipitation_observed']
        ]);
        Log::debug('addPrecipitationData function in model(id) (' . $precipitation->id .'), input: ' . $input['precipitation_date'] . '/' . $input['precipitation_time'] . '/' . $input['location_id'] . '/' . $input['precipitation_observed'] . '!');

        return $precipitation->id;
    }

}
