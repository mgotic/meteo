<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

use Carbon\Carbon;

class Location extends Model
{
    protected $fillable = ['namesc','name','gps','masl','country'];
    protected $connection = 'sqlite_data';

    public static function getOrCreateLocation($name, $gps = null, $masl = null, $country = 'HRV', $create = true)
    {
        $gradsc = strtolower($name);
        $gradsc = preg_replace("/[^a-z0-9_\s-]/", "", $gradsc);
        $gradsc = preg_replace("/[\s-]+/", " ", $gradsc);
        $gradsc = preg_replace("/[\s_]/", "-", $gradsc);
        
        if($create) {
            $location = Location::firstOrCreate(
                ['namesc' => $gradsc],
                ['namesc' => $gradsc, 'name' => $name, 'gps' => $gps, 'masl' => $masl, 'country' => $country]
            );
        } else    {
            $location = Location::where('namesc', 'like', $gradsc)->firstOrFail();
        }
        Log::debug('getOrCreateLocation function in Location model, input: ' . $name . '/' . $gradsc . '/' . $gps . '/' .  $masl . '/' . $country .  '/' . $create  . ', output: ' . $location->id . '.');
        
        return $location;
    }
}
