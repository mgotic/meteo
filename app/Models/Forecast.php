<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Forecast extends Model
{
    protected $fillable = ['forecast_type_id','naslov','tekst','nastao','forecest_for','forecest_from'];
    protected $connection = 'sqlite_data';

    public static function addForecastData($input)
    {
        $forecast = Forecast::firstOrCreate([
            'forecast_type_id' => $input['forecast_type_id'], 
            'forecest_for' => $input['forecest_for'], 
            'forecest_from' => $input['forecest_from']
        ],[
            'forecast_type_id' => $input['forecast_type_id'], 
            'naslov' => $input['naslov'],
            'tekst' => $input['tekst'],
            'nastao' => $input['nastao'],
            'forecest_for' => $input['forecest_for'], 
            'forecest_from' => $input['forecest_from']
        ]);
        Log::debug('addForecastData function in Forecast(id) (' . $forecast->id .'), input: ' . $input['forecast_type_id'] . '/' . $input['forecest_for'] . '/' . $input['forecest_from'] . '/' . $input['naslov'] . '/' . $input['nastao'] . '!');

        return $forecast->id;
    }
}
