<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Weather extends Model
{
    protected $fillable = ['weather_date','weather_time','location_id','station_type_id','wind_direction','wind_speed','air_temperature','relative_humidity','atmospheric_pressure','pressure_tendency','weather_condition_id','weather_icon'];
    protected $connection = 'sqlite_data';

    public static function addWeatherData($input)
    {
        $weather = Weather::firstOrCreate(
            ['weather_date' => $input['weather_date'], 'weather_time' => $input['weather_time'], 'location_id' => $input['location_id']],
            ['weather_date' => $input['weather_date'], 'weather_time' => $input['weather_time'], 'location_id' => $input['location_id'], 
             'station_type_id' => $input['station_type_id'], 'wind_direction' => $input['wind_direction'], 'wind_speed' => $input['wind_speed'], 
             'air_temperature' => $input['air_temperature'], 'relative_humidity' => $input['relative_humidity'], 'atmospheric_pressure' => $input['atmospheric_pressure'],
             'pressure_tendency' => $input['pressure_tendency'], 'weather_condition_id' => $input['weather_condition_id'], 'weather_icon' => $input['weather_icon']]
        );
        Log::debug('addWeatherData function in model, input: ' . $input['weather_date']. '/' . $input['weather_time'] . $input['location_id'] .', output: ' . $weather . '.');

        return $weather->id;
    }
}
