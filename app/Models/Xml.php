<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class Xml extends Model
{
    protected $fillable = ['loaded','content'];
    protected $connection = 'sqlite';

    public static function exists($sha1)
    {
        $counter = Xml::where('sha1','like', $sha1)->count();
        return $counter;
    }
        
    public static function setXmlLoaded($id, $value, $truncate = false)
    {
        $result = Xml::find($id);
        $result->loaded = $value;
        if($truncate)   {
            $result->content = ' ';
        }
        $result->save();
        Log::debug('setXmlLoaded function in model, input: ' . $id . ', value: ' . $value . ', output: ' . $result->id . '.');
        return 1;
    }

    public static function resetXmlLoaded($id)
    {
        $result = Xml::where('source_id', '=', $id)
                ->where('loaded','!=', 0)->orWhere('loaded','!=', 1)
                ->update(['loaded' => 0]);
        Log::debug('resetXmlLoaded function in model Xml, input: ' . $id . ', output: ' . $result);
        return $result;
    }

}
