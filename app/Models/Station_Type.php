<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Station_Type extends Model
{
    protected $fillable = ['code','desc'];
    protected $connection = 'sqlite_data';
    protected $table = 'station_types';

    public static function getStationType($podaci)
    {
        $station_type = station_type::firstOrCreate(
            ['code'=>$podaci],
            ['code'=>$podaci]
        );
        Log::debug('getStationType function in model, input: ' . $podaci . ', output: ' . $station_type->id . '.');
        return $station_type->id;
    }
}
