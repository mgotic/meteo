<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Snow extends Model
{
    protected $fillable=['snow_date','snow_time','location_id','snow_amount', 'snow_amount_new'];
    protected $connection = 'sqlite_data';

    public static function addSnowData($input)
    {
        $snow = Snow::firstOrCreate([
            'snow_date' => $input['snow_date'], 
            'snow_time' => $input['snow_time'], 
            'location_id' => $input['location_id']
        ],[
            'snow_date' => $input['snow_date'], 
            'snow_time' => $input['snow_time'], 
            'location_id' => $input['location_id'],
            'snow_amount' => $input['snow_amount'],
            'snow_amount_new' => $input['snow_amount_new']
        ]);
        Log::debug('addSnowData function in Snow(id) (' . $snow->id .'), input: ' . $input['snow_date'] . '/' . $input['snow_time'] . '/' . $input['location_id'] . '/' . $input['snow_amount'] . '/' . $input['snow_amount_new'] . '!');

        return $snow->id;
    }
}
